#include <stdio.h>
#include <cufft.h>

cufftHandle plan_R2C, plan_C2R;

extern "C" void initCUFFT(int nx, int ny, int nz, void *stream)
{
    cufftPlan3d(&plan_R2C, nx, ny, nz, CUFFT_R2C);
    cufftSetStream(plan_R2C, (cudaStream_t)stream);

    cufftPlan3d(&plan_C2R, nx, ny, nz, CUFFT_C2R);
    cufftSetStream(plan_C2R, (cudaStream_t)stream);
}

extern "C" void launchCUFFT(float * idata, float * odata, cufftType type)
{
    if(type == CUFFT_R2C)
    {
        cufftExecR2C(plan_R2C, (cufftReal*)idata, (cufftComplex*)odata);
    }
    else if(type == CUFFT_C2R)
    {
        cufftExecC2R(plan_C2R, (cufftComplex*)idata, (cufftReal*)odata);
    }
    else
    {
        printf("Invalid transform type!\n");
    }
}

extern "C" void cleanCUFFT()
{
    cufftDestroy(plan_R2C);
    cufftDestroy(plan_C2R);
}
