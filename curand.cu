#include <stdio.h>
#include <curand.h>

curandGenerator_t random_number_generator;

// initialize random number generator
extern "C" void initCURAND(void *stream)
{
    int status;

    // Create generator
    status = curandCreateGenerator(&random_number_generator, CURAND_RNG_PSEUDO_PHILOX4_32_10);

    // Set CUDA stream
    status |= curandSetStream(random_number_generator, (cudaStream_t)stream);

    // Set seed
    status |= curandSetPseudoRandomGeneratorSeed(random_number_generator, 2);

    if (status != CURAND_STATUS_SUCCESS) {
        printf ("curand init failure!\n");
        exit (EXIT_FAILURE);
    }
}

// Fill d_buffer with num random numbers with normal distribution
extern "C" void launchCURAND_normal(float * d_buffer, int num)
{
    int status;

    // Generate num random numbers
    status = curandGenerateNormal(random_number_generator, d_buffer, num, 0, 1);

    if (status != CURAND_STATUS_SUCCESS) {
        printf ("curand fill failure!\n");
        exit (EXIT_FAILURE);
    }
}

// Fill d_buffer with num random numbers with uniform distribution
extern "C" void launchCURAND_uniform(float * d_buffer, int num)
{
    int status;

    // Generate num random numbers
    status = curandGenerateUniform(random_number_generator, d_buffer, num);

    if (status != CURAND_STATUS_SUCCESS) {
        printf ("curand fill failure!\n");
        exit (EXIT_FAILURE);
    }
}

// cleanup
extern "C" void cleanCURAND()
{
    int status;

    status = curandDestroyGenerator(random_number_generator);

    if (status != CURAND_STATUS_SUCCESS) {
        printf ("curand clean failure!\n");
        exit (EXIT_FAILURE);
    }
}
